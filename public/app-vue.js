Vue.createApp({
    // Questa viene eseguita quando viene montato il componente
    mounted() {
        // Chiamo un metodo che carica gli utenti
        this.fetchUsers();
    },

    // Ci permette di dichiarare dei metodi accessibili dal template
    methods: {
        // Metodo che prende gli utenti dal DB in HTTP
        async fetchUsers() {
            this.isLoading = true;
            var userResponse = await fetch('/api/users');
            var usersJson = await userResponse.json();
            this.users = usersJson; // Assegno la risposta alla proprietà users del modello dati
            this.isLoading = false;
        },

        // Metodo che inserisce l'utente in DB tramite HTTP[POST]
        async addUser(fullname, birthday) {
            await fetch('/api/users', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    fullname: fullname,
                    birthday: birthday
                })
            });

            // Ricarico la lista utenti
            this.fetchUsers();

            // Resetto le caselle di testo
            this.newBirthday = '';
            this.newUserName = '';

        },

        // Metodo che elimina l'utente dal DB tramite HTTP[DELETE]
        async deleteUser(id) {
            await fetch('/api/users/' + id, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                },
            });

            // Ricarico la lista utenti
            this.fetchUsers();

        }
    },

    // Template verrà utilizzato per renderizzare l'html dentro il tag #app
    template: /* alt + 96 */ `
    <div>
        <input placeholder = "Nome utente" v-model = "newUserName"/>
    </div>

    <div>
        <input type = "date" v-model = "newBirthday"/>
    </div>

    <div>
        <button @click = "addUser(newUserName, newBirthday)">Aggiungi utente</button>
    </div>

    <svg v-if = 'isLoading' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin:auto;background:#fff;display:block;" width="50px" height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
        <circle cx="50" cy="50" fill="none" stroke="#1d3f72" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138">
            <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1s" values="0 50 50;360 50 50" keyTimes="0;1"></animateTransform>
        </circle>
    </svg>

    <div v-if = "users.length <= 0 && !isLoading"> Non ci sono utenti </div>

    <ul>
        <li v-for = "user in users">
            <button @click = "deleteUser(user.id)">X</button>
            <span> {{user.fullname}} </span>
        </li>
    </ul>
    `,

    // Il metodo data serve a definire il modello dati, in particolare l'oggetto tornato dal metodo
    data() {
        return {
            isLoading: false, // Flag di caricamento
            noUsers: true, // Indica se ci sono utenti
            newBirthday: '', // Data di nascita per il nuovo utente
            newUserName: '', // Nome del nuovo utente
            users: [], // Utenti presi dal DB
        }
    }
}).mount('#app');