<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request; // Mi serve per l'oggetto Request
use Illuminate\Http\Response; // Mi serve per l'oggetto Response

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    // Aggiorno il messaggio nel database
    public function update(Request $request, $id){
        $body = $request->input('body');

        $result = app('db')->update("UPDATE messages SET body = '$body' WHERE id = $id");

        // Verifico se il messaggio esisteva ed è stato aggiornato
        // Controllo il numero di record aggiornati che dovrebbe essere = 1
        if($result != 0){
            return new Response(null, 200);
        }
        else {
            return new Response(null, 404);
        }
    }
}