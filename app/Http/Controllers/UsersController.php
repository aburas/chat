<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; // Mi serve per l'oggetto Request
use Illuminate\Http\Response; // Mi serve per l'oggetto Response

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    // Esegue una query select su users e ritorna il json di questo elenco
    public function list()
    {
        $results = app('db')->select("SELECT * FROM users");
        return $results;
    }

    // Esegue una query di tipo insert su users e ritorna lo status code 200
    public function add(Request $request)
    {
        $fullname = $request->input('fullname');
        $birthday = $request->input('birthday');

        // Se compilato eseguo la query di inserimento con la data
        if ($birthday != null) {
            $results = app('db')->insert("INSERT INTO users (fullname, birthday, insertDate) VALUES ('$fullname', '$birthday', now())");
        }
        // Altrimenti eseguo l'inserimento senza data
        else {
            $results = app('db')->insert("INSERT INTO users (fullname, insertDate) VALUES ('$fullname', now())");
        }

        return new Response(null, 200);
    }

    // Esegue una query di tipo delete su users e ritorna lo status code 201
    public function delete(Request $request, $id)
    {
        $results = app('db')->delete("DELETE FROM users WHERE id = $id");
        return new Response(null, 201);
    }
}
