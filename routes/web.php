<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// api/users[GET] => Ritorna la lista degli utenti JSON
$router->get('/api/users', 'UsersController@list');

// api/users[POST] => aggiunge un utente nel database
$router->post('/api/users', 'UsersController@add');

// api/users[DELETE] => rimuove un utente dal database
$router->delete('/api/users/{id}', 'UsersController@delete');

// api/messages[PUT] => aggiorna un messaggio nel database
// api/messages/3
$router->put('/api/messages/{id}', 'MessagesController@update');

?>